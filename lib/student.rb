class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    @courses << course unless @courses.include?(course)
    course.students << self
  end


  def course_load
    courses_hash = Hash.new(0)
    @courses.each do |course|
      courses_hash[course.department] += course.credits
    end
    courses_hash
  end

  def enroll(course)
    raise "error" if has_conflict(course)
  end

  # this isn't raising an error, and I'm not sure why; other tests suggest
  # the enroll method contains the problem, since all the tests dependent on
  # has_conflict and conflicts_with pass. I'm moving on for now and will come
  # back after I give it some thought. 

  def has_conflict(course)
    @courses.any? do |old_course|
      course.conflicts_with?(old_course)
    end
  end

end
